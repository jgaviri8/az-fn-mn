package mn.az.fn;

import org.reactivestreams.Publisher;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import reactor.core.publisher.Mono;

@Controller("/greetings/spanish")
public class OtroController {

    @Produces(MediaType.TEXT_PLAIN)
    @Get("/{name}")    
    public Publisher<HttpResponse<String>> index(@PathVariable Publisher<String> name) {
      return Mono.from(name)
          .map(item -> HttpResponse.ok(item + ", bienvenido a Azure Functions con Java y micronaut"));
    }

    @Post
    public Publisher<HttpResponse<SampleReturnMessage>> postMethod(@Body Publisher<SampleInputMessage> inputMessage){
      return Mono.from(inputMessage).map(item -> {
        SampleReturnMessage retMessage = new SampleReturnMessage();
        retMessage.setReturnMessage("Hola " + item.getName() + ", gracias por enviar el mensaje");
        return HttpResponse.ok(retMessage);
      });
    }
}
