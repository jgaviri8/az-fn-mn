package mn.az.fn;

import org.reactivestreams.Publisher;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import reactor.core.publisher.Mono;

@Controller("/greetings/english")
public class MnAzFnController {

    @Produces(MediaType.TEXT_PLAIN)
    @Get("/{name}")
    public Publisher<HttpResponse<String>> index(@PathVariable Publisher<String> name) {
      return Mono.from(name)
          .map(item -> HttpResponse.ok(item + ", welcome to Java with micronaut"));
    }

    @Post
    public Publisher<HttpResponse<SampleReturnMessage>> postMethod(@Body Publisher<SampleInputMessage> inputMessage){
      return Mono.from(inputMessage).map(item -> {
        SampleReturnMessage retMessage = new SampleReturnMessage();
        retMessage.setReturnMessage("Hello " + item.getName() + ", thank you for sending the message");
        return HttpResponse.ok(retMessage);
      });
    }
}

@Introspected
class SampleInputMessage{
    private String name;

    public SampleInputMessage() {
    }

    public SampleInputMessage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}

@Introspected
class SampleReturnMessage{
    private String returnMessage;
    public String getReturnMessage() {
        return returnMessage;
    }
    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }
}
