FROM gitpod/workspace-full

# Install custom tools, runtime, etc.
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash \
    && sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-$(lsb_release -cs)-prod $(lsb_release -cs) main" > /etc/apt/sources.list.d/dotnetdev.list' \
    && sudo apt-get update \
    && sudo apt-get install azure-functions-core-tools-4